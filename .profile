# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n || true
export PATH=$PATH:/storage/bin
alias scd=". scd"
alias ss="sssh"

# Tab completion for tools
for line in `ls -1 /storage/bin`; do  complete -W "`ls -1 /storage/shared | grep -v components`" $line; done
